<?php
//**************************************************************************************************

// Switch and Call
switch ( $requestMethod ) {
    case 'GET' :
        $txtSQL = "SELECT * FROM users ";
        echo json_encode( DB::run($txtSQL)->fetchAll() );
        break;
    case 'POST' :
        //$txtSQL = "INSERT INTO users SET User_Name = ?, User_Email = ? ";
        //DB::run($txtSQL, [ $requestBody["User_Name"], $requestBody["User_Email"] ]);
        $txtSQL = "INSERT INTO users SET " . $setFields;
        DB::run($txtSQL, $setValues);
        echo json_encode( array( 'lastInsertId' => DB::lastInsertId() ) );
        break;
    case 'PUT' :
        //$txtSQL = "UPDATE users SET User_Name = ?, User_Email = ? WHERE User_ID = ? ";
        //DB::run($txtSQL, [ $requestBody["User_Name"], $requestBody["User_Email"], $requestParams[1] ]);
        $txtSQL = "UPDATE users SET " . $setFields . " WHERE User_ID = ? ";
        $setValues[] = $requestParams[1];
        DB::run($txtSQL, $setValues);
        break;
    case 'DELETE' :
        $txtSQL = "DELETE FROM users WHERE User_ID = ? ";
        DB::run($txtSQL, [ $requestParams[1] ]);
        break;
    case 'OPTIONS' :
        try {
            $txtSQL = "SELECT COUNT(*) AS RecCount FROM users ";
            $stmt = DB::instance()->prepare($txtSQL);
            $stmt->execute([]);
            $rowSQL = $stmt->fetch();
            echo $rowSQL["RecCount"];
        } catch (PDOException $e) {
            http_response_code(500);
            die($e->getMessage());
        }
        break;
}

//**************************************************************************************************
