<?php
//**************************************************************************************************

require_once("api_config.php");
require_once("api_db.php");

//**************************************************************************************************
//**************************************************************************************************
//**************************************************************************************************

// HTTP method
$requestMethod = $_SERVER['REQUEST_METHOD'];

// Parameters
$requestParams = array();
if ( isset($_REQUEST['params']) ) {
    $requestParams = explode( '/', trim($_REQUEST['params'],'/') );
}

// Body of the request
$requestBody = array();
if ( in_array( $requestMethod, array('POST','PUT','DELETE') ) )
{
    $requestBody = json_decode( file_get_contents('php://input'), true );
}

// Build the SET part of the SQL command
$setFields = '';
$setValues = array();
foreach ( $requestBody as $key => $value ) {
    $setFields .= $key . '=?,';
    $setValues[] = $value;
}
$setFields = substr( $setFields, 0, -1 );

// Test
//var_dump($requestMethod);
//var_dump($requestParams);
//var_dump($requestBody);
//var_dump($setFields);
//var_dump($setValues);

// Switch and Call
if ( !isset( $requestParams[0] ) ) {
    exitMsg( 'Wrong route.' );
}
switch ( true ) {
    case $requestParams[0] == 'demo' :
        require_once("demo.php");
        break;
    case $requestParams[0] == 'users' :
        require_once("users.php");
        break;
    default :
        exitMsg( 'Unknown route: ' . $requestParams[0] );
        break;
}

//**************************************************************************************************

function exitMsg( $msg ) {

    $obj = array();
    $obj["Status"] = $msg;
    echo json_encode( $obj );
    http_response_code(500);
    exit;

}

//**************************************************************************************************
