<?php
//**************************************************************************************************
// PDO wrapper
//**************************************************************************************************

class DB
{
    protected static $instance = null;

    // Create instance
    public static function instance()
    {
        if (self::$instance === null)
        {
            $opt  = array(
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => TRUE,
            );
            $dsn = 'mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset='.DB_CHAR;
            try {
                self::$instance = new PDO($dsn, DB_USER, DB_PASS, $opt);
            } catch (PDOException $e) {
                http_response_code(500);
                die($e->getMessage());
            }
        }
        return self::$instance;
    }

    // Call standard PDO methods
    public static function __callStatic($method, $args)
    {
        return call_user_func_array(array(self::instance(), $method), $args);
    }

    // Call run == prepare + execute
    public static function run($sql, $args = [])
    {
        try {
            $stmt = self::instance()->prepare($sql);
            $stmt->execute($args);
            return $stmt;
        } catch (PDOException $e) {
            http_response_code(500);
            die($e->getMessage());
        }
    }

}

//**************************************************************************************************
